---
title: Support
weight: 12
---

# Support

### **What is Evercam’s SLA?**
See [Evercam SLA]({{< relref "/other-faqs/sla" >}})

### **How do I request full frame rate footage / incident clip of a historical event that occurred on my site?**
A footage request is logged via support@evercam.io. The request should include that the 
following:
- Date 
- Time period
- Details of the area of focus (i.e. marked up image).

Evercam recommends using the cloud recording functionality to narrow down the time period 
required as Full Frame Rate processing time will depend on duration.

### **How long is footage stored?**
We keep the local hard drive HDDs for 3 years. The cloud footage accessible via Evercam 
dashboard is stored and accessible for 3 years following project completion. 

Recordings will not be deleted without written instruction from the Client

### **How do I submit a support request / contact support team?**
Customer queries can be logged 24 hours a day, seven days a week, 365 days a year via 
support@evercam.io  Please use this email address in addition to, or in place of your Evercam 
Account Manager’s address for the fastest response. Emailing support@evercam.io is the most 
efficient way to open a ticket.

Alternative means of raising a ticket include contacting our customer success team, your 
account manager, or via our chatbot on our landing page.

All incoming customer inquiries are answered directly by our support team with support 
management monitoring open incidents and making available appropriate resources to facilitate 
the resolution of support cases. This process provides a formal mechanism to deal with more 
complex issues and ensures that Evercam's high standards of customer service are maintained.

As part of our continuous improvement plan, we are always keen to understand how our Clients 
feel about the service received when interacting with Evercam. Every time we close a case, we 
ask for feedback on the service you’ve received from the Evercam support teams. This is used to 
monitor and evaluate the service we deliver and to ensure we improve your experience with 
Evercam.

### **How do I book an onboarding / project kickoff / project wrap-up? Who do I contact for support and product feedback?**
You can email your Customer Success Manager (CSM) directly.

### **How do I share with new users?**

1. Click on the *Sharing* tab along the top of the page.
2. Enter the email address and a short message to the contacts you would like to add to the 
camera.
3. You can choose between *Read-only* or *Read Only + Share* rights.
4. You can add as many users as you like. 
5. Alternatively, feel free to send us a list of all of the email addresses you would like to 
add to the camera and we will take care of it for you.

{{< hint danger >}}
Full sharing [tutorial](https://evercam.io/tutorials/sharing-the-camera/)
{{< /hint >}}