---
title: Admin
weight: 13
---

# Admin

### **How can a Project Admin view user usage?**
1. You can request access to Usage via your CSM or by emailing support@evercam.io.
2. Once access has been granted, you can click on your name on the top right corner of the screen.
3. Then click on *Company*.
4. You will be able to see usage statistics by user.
5. You can also export reports if needed.

### **Who can request video footage?** 
Only users with *Read Only & Share* access can request local footage. 

### **Do you have an open API?**
Yes, see the [Developer doc](https://docs.evercam.io/docs/).

### **How do I name my project / camera in the Evercam system? Is there a standard naming convention?**
None, this will depend on how you would like to name it.
