---
title: Drone
weight: 4
bookCollapseSection: true
---
# Drone

{{< hint info >}}
Drone allows you to visualise a 3D reconstruction of your site from a drone mapping (photogrammetry).
{{< /hint >}}

<p align="center">
<img src="/images/drone.png" width = 600 />
</p>

## Features

1. To explore the view,you can use the navigation tools to enter full screen, zoom in/out & and reset the view. You can use your mouse or keypad to rotate around the model.

<p align="center">
<img src="/images/drone_navigation_tools.png" width = 600 />
</p>

2. You can quantify distances, heights and areas (with +/- 5mm margin of error) using the 
*Measure* tool.

<p align="center">
<img src="/images/drone_measurement.gif" width = 600 />
</p>

3. You can access all Evercam features through the live camera view by clicking on the camera 
markers.

<p align="center">
<img src="/images/open_camera_view.gif" width = 600 />
</p>

4. To highlight information for other users, you can create (and hide) annotations on the model 
using *Tagging* tool.

<p align="center">
<img src="/images/drone_tagging.gif" width = 600 />
</p>

<p align="center">
<img src="/images/hide_tags.gif" width = 600 />
</p>

<p align="center">
<img src="/images/delete_tag.gif" width = 600 />
</p>

5. To track progress on site, you can use *Compare* tool and choose between comparing two drone 
flights (available when there is more than one flight), or comparing with the BIM Model. 

6. To see the site from the top, you can use the *2D* tool.

<p align="center">
<img src="/images/2d_mode.gif" width = 600 />
</p>

7. To check out the surrounding buildings, enable the *OSM building* feature.

8. To access the weather information, click on the *Weather* button and choose the specific 
date you would like to view.

9. To share the image with other users, you can use the *Edit* tool to capture a screenshot and 
place text or drawings.

<p align="center">
<img src="/images/drone_edit.png" width = 600 />
</p>

10. You can change the quality (SD, HD, 4K) of the drone image.

<p align="center">
<img src="/images/drone_settings.png" width = 600 />
</p>

11. You can change the background map style.

<p align="center">
<img src="/images/drone_map_style.png" width = 600 />
</p>

12. If you would like to see the drone flight clearly without occlusions, you can hide the 
camera markers and names.

<p align="center">
<img src="/images/hide_camera_markers.gif" width = 600 />
</p>

13. You can also access the Gate Report summary by clicking on the Gate Report camera markers. 
(*Note*: This is only applicable if you have Gate Report in your project.)

<p align="center">
<img src="/images/open_GR_from_drone.gif" width = 600 />
</p>

{{< hint danger >}}
Watch the [Full Video Tutorial](https://vimeo.com/778539295) for more information.
{{< /hint >}}