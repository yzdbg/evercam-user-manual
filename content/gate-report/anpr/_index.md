---
title: ANPR
weight: 9
bookCollapseSection: false
---
# ANPR

{{< hint info >}}
ANPR stands for Automatic Number Plate Recognition, also known as License Plate Recognition 
(LPR) in some regions. It is a technology used to automatically capture, read, and process 
alphanumeric characters from the license plates and convert them into machine-readable text 
using a specific camera. 
{{< /hint >}}

<p align="center">
<img src="/images/anpr.png" width = 600 />
</p>

With a number of vehicles coming in and out of a site, security issues may arise and tracking vehicles manually can be a piece of work. ANPR enhances security and assists logistics in construction sites.

## How to Access ANPR

1. Select your project and the camera from your Project Dashboard, and then click on *ANPR* on the top menu bar to view the detected vehicles and license plates.

2. If you want to share the results, you can do so in 2 ways:

- Option 1:
    - Clicking on *Copy to clipboard* button, and
    - Pasting into an Excel/ Google sheet.

- Option 2:
    - Clicking on *Export as* button, and
    - Click on *CSV* to download.

<p align="center">
<img src="/images/anpr_csv.gif" width = 600 />
</p>

3. If you want to show specific results, you can filter them by:
- plate number 
- period  
- direction

4. If you want to see whether the vehicles coming in also go out of the site, the *Matches* tab shows the arrival and departure times for each vehicle. 
- You can sort by arrival or departure time. 
- You can view the event in *Recordings*.   

<p align="center">
<img src="/images/anpr_matches.png" width = 600 />
</p>

5. If you want to see the summary of unique license plates, go to the *Plates* tab which will show: 
- the thumbnail and the plate number
- the total number of detections of each vehicle
- the first and last sighting of each vehicle
- a link to view the event in *Recordings*  

<p align="center">
<img src="/images/anpr_plates.png" width = 600 />
</p>

6. You can choose to display from 20, 100, 200, 500, or 1000 rows per page.

<p align="center">
<img src="/images/anpr_page_display.png" width = 400 />
</p>

{{< hint warning>}}
Note: ANPR requires a dedicated camera to track vehicles' plates and provide a report. This is separate from the Gate Report camera.
{{< /hint >}}