---
title: Gate Report
weight: 6
bookCollapseSection: true
---

# Gate Report

{{< hint info >}}
The Gate Report tool utilizes AI to identify all vehicles that enter & exit 
your site and provides you with an in-app & downloadable report.
{{< /hint >}}

On the Project Dashboard, you can click on *Gate Report* at the left navigation bar to access the features.

<p align="center">
<img src="/images/gate_report.png" width = 600 />
</p>

## Overview

The overview tab shows all the events (entries & exits) from your project.

<p align="center">
<img src="/images/GR_overview.gif" width = 600 />
</p>

1. You can sort by type of vehicle.

2. You can select last week, last month or overall.

## Entries

The entries tab shows a list of all the events, along with vehicle type, thumbnail, and arrival time, that enter your site.

<p align="center">
<img src="/images/GR_entries.gif" width = 600 />
</p>

1. You can sort by type of vehicle and have the option to hide thumbnails.

2. You can select the specific date and time by using the calendar.

3. You can *copy to clipboard* or, 

4. You can export the report as a PDF or CSV file.

## Exits

The exits tab shows a list of all the events, along with the vehicle type, thumbnail, and departure time, that leave your site.

<p align="center">
<img src="/images/GR_exits.png" width = 600 />
</p>

1. You can sort by type of vehicle and have the option to hide thumbnails.

<p align="center">
<img src="/images/sort_by_vehicle.gif" width = 300 />
</p>

2. You can select the specific date and time by using the calendar.

3. You can *copy to clipboard* or, 

4. You can export the report as a PDF or CSV file.

<p align="center">
<img src="/images/export_as.png" width = 200 />
</p>

## Matches

{{< hint warning >}}
Gate Report v2.0 no longer has matching. The previous version of Gate Report shows the matching tab, and therefore, those customers who availed of the product before v2.0 can still see this feature. 
{{< /hint >}}

The matches tab shows a list of events that both enter and leave the site. Aside from showing the vehicle type, the arrival and departure times, it also shows the total time any vehicle spent on site.

<p align="center">
<img src="/images/GR_matches.gif" width = 600 />
</p>

1. You can sort by type of vehicle and have the option to hide thumbnails.

2. You can select the type of event (entry/exit/all) to view.

3. You can select the specific date and time by using the calendar.

4. You can *copy to clipboard* or, 

5. You can export the report as a PDF or CSV file.

6. You can watch the video clip from the thumbnail.

{{< hint warning >}}
WARNING: We can detect road vehicles through the AI model but we do not manually review the footage to add missed road vehicles.

We do not match road vehicles. Road vehicles look very similar and a lot of them enter and leave the site, it's quite hard to tell which is which.
{{< /hint >}}

{{< hint danger >}}
Watch these videos for more information:

[Full Video Tutorial](https://vimeo.com/512971478)

[Feature Overview](https://evercam.com/features/gate-report)
{{< /hint >}}