---
title: Brain
weight: 7
---

# Everbrain

{{< hint info >}}
This AI object detection tool is still in beta phase.
{{< /hint >}}

The brain fuction can be found within the *Recordings* tab.

<p align="center">
<img src="/images/brain.png" width = 600 />
</p>

It can detect various vehicles and workers on site.

<p align="center">
<img src="/images/brain_function.gif" width = 600 />
</p>

<p align="center">
<img src="/images/everbrain.gif" width = 600 />
</p>