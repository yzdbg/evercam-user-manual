---
title: Xray
weight: 6
---

# Xray

{{< hint info >}}
A tool within *Recordings* that allows you to scan the image to compare completed work against the current status of the project.
{{< /hint >}}

<p align="center">
<img src="/images/xray.png" width = 600 />
</p>

1. To start comparing, you can select the *Xray* Icon in the bottom right corner.

2. If you want to select a specific *Recordings* and *Xray* date and hour, use the Calendar on the side panel.

3. You can drag the Xray image and see the magic happening. You can also resize the XRay region to capture the exact area you are interested in viewing.

{{< hint danger >}}
Watch the [Tutorial video](https://vimeo.com/552767816) to find out more.

Watch the [New Feature video](https://vimeo.com/847214630/c1a7507d33).
{{< /hint >}}