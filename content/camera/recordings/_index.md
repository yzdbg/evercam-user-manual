---
title: Recordings
weight: 5
bookCollapseSection: true
---

# Cloud Recordings

{{< hint info >}}
With Evercam construction cameras, everything that happens on your construction site 
is recorded locally, in high resolution, to the cloud.
{{< /hint >}}

<p align="center">
<img src="/images/recordings.png" width = 600 />
</p>

Your entire construction project is recorded in 4K and easily accessible from the *Recordings* 
tab in the Project dashboard.

Our easy-to-use interface allows you to view recordings by date & time. You can create a clip 
and store it in your Archive without having to wait to access your footage.

## How to Access

1. To gain access, you can select the *Recordings* Tab at the top of the dashboard.

2. If you want to view a specific date, you can use the *Calendar* on the right side.

3. You can also choose the specific hour to view.

4. Then, you can select a specific time within that hour thru the *Timeline* at the bottom of 
the viewer. Click either the *backward* (<) and *forward* (>) buttons to view the recording 
frame by frame. Click the *Play* button to view the recording.

5. You can choose the Playback Speed, which ranges from 0.25 - 2.5.

6. If you want to Save/ Download/ Archive/ Share the video clip, you can click the *Export As* 
button, then input the title, start date, start time, and duration. Then click *Create Clip* to 
finish.

{{< hint danger >}}
Watch the full Cloud Recordings video [Tutorial](https://vimeo.com/showcase/8254136/video/522374334) for more information.
{{< /hint >}}

## Request for Full Frame Rate Local Recordings

1. To make a request, you can use *Recordings* to identify the time-frame in which the incident occurred. The larger the time frame, the longer it takes to retrieve the footage.

2. You can make a footage request via support@evercam.io. 

3. Your request should include the following:
- Date 
- Time period
- Details of the area of focus (i.e. marked up image).
