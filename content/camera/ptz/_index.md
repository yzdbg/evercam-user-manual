---
title: PTZ
weight: 11
bookCollapseSection: false
---

# PTZ

{{< hint info >}}
PTZ  (Pan-Tilt-Zoom) camera allows for wider coverage from any angle and higher zoom quality for any detail.
{{< /hint >}}

A PTZ camera will be reflected on the camera list with the letters **PTZ** as part of the camera name.

These cameras have separate controls found on the upper right side of the window when you're in *Live View*.

<p align="center">
<img src="/images/ptz_controls.png" width = 600 />
</p>