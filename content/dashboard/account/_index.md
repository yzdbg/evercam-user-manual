---
title: Account
weight: 1
---

# Account

<p align="center">
<img src="/images/Account_Access.png" width = 300 />
</p>

## Create an Evercam Account & Access your project

1. In order to gain access to a project, the project owner must share access with your email
 (that will be used to create your account).

2. Once access is shared, you will receive an automated email (search Spam just in case!) from 
no-reply@evercam.io with the Subject line *Construction Evercam has shared the camera*  to 
create your account. 

3. If you do not receive it, go to this [link](https://dash.evercam.io/v2/users/signin).

4. Once you’ve signed up, you can log in with your credentials.

5. When logged in, you should see the most recent image of any project/cameras that have been 
shared with you on the right side of the screen.

## Single Sign-On

If you have a Google or Microsoft account, you may use the same login credentials for your Evercam account.

## Two-Factor Authentication

Two-factor authentication (TFA) is best implemented via SSO. You can use SSO provider Google or Microsoft and enable TFA from there.