---
title: Company Admin
weight: 2
---

# Company Admin

{{< hint info >}}
Evercam users only have access to the project(s) they are invited to. 
{{< /hint >}}

<p align="center">
<img src="/images/company_admin.png" width = 600 />
</p>

Box A above shows the available projects and box B shows the number of cameras within each project. 

## Navigating your Project Dashboard

To view your project, follow the steps below: 

1. You can click on the desired project in Company Admin.

2. Once in the Project Dashboard, you can access the following:
 - Live view + Camera Features: Click on desired camera (Box A)
 - Gate Report: Box B
 - BIM: Box C
 - Drone: Box D
 - 360: Box E
 - Archives: Box F (all project saved images/time lapses/video clips/gifs/etc)

<p align="center">
<img src="/images/Navigating_dashboard.png" width = 600 />
</p>

3. Map View will show you the location of your project in a map style.

<p align="center">
<img src="/images/map_view.png" width = 600 />
</p>

4. You can manage your account at the upper right hand corner of the dashboard, where you will also see the help button and notifications. Whenever there are new notifications, a blue circle will appear next to the bell icon.

<p align="center">
<img src="/images/Account_settings.png" width = 600 />
</p>

<p align="center">
<img src="/images/notifications.png" width = 200 />
</p>

5. By clicking the *Help* button, you will be able to access *Live Chat* and *Tutorials*.

<p align="center">
<img src="/images/help_button.png" width = 200 />
</p>

<p align="center">
<img src="/images/live_chat.png" width = 200 />
</p>

<p align="center">
<img src="/images/tutorials.png" width = 500 />
</p>

6. When you click on *Settings*, you will be brough to your account info.

<p align="center">
<img src="/images/account_info.png" width = 600 />
</p>

7. By clicking on *Company*, you will find information on the active users, events and projects that you have access to.

<p align="center">
<img src="/images/active_users.png" width = 600 />
</p>

<p align="center">
<img src="/images/events.png" width = 600 />
</p>

<p align="center">
<img src="/images/projects.png" width = 600 />
</p>

8. If you'd like to include a widget on your Company website, the embed codes for *Live View Widget*, *Recordings Widget* and *BIM Compare Widget* are available under *Settings*.

<p align="center">
<img src="/images/widgets.png" width = 800 />
</p>

9. You can choose either Dark or Light Mode depending on your preference.

<p align="center">
<img src="/images/light_mode.png" width = 600 />
</p>

<p align="center">
<img src="/images/dark_mode.png" width = 600 />
</p>