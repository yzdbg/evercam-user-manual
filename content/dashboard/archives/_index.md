---
title: Archives
weight: 5
---

# Archives

{{< hint info >}}
A repository of all cloud recording clips, time-lapse videos, edited images and compare gifs.
{{< /hint >}}

<p align="center">
<img src="/images/archives.png" width = 600 />
</p>

We want our platform to be a single-source of truth where you can find all your saved files for your projects.

## How to Access

1. If you want to access camera-specific archives, go to the *Archives* Tab on the camera 
toolbar at the top of the dashboard.

2. If you want to access all project archives, go to the *Archives* link on the left project navigation bar.

3. You can download and share media directly from your Evercam Archives.

4. You can filter your search by camera, by file type, by who created the file, by date and also sort the archives alphabetically from A-Z or Z-A.

<p align="center">
<img src="/images/archive_filters.png" width = 800 />
</p>

{{< hint danger >}}
Watch the video [Tutorial](https://vimeo.com/435099805)
{{< /hint >}}