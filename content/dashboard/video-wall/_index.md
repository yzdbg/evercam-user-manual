---
title: Video Wall
weight: 3
---

# Video Wall

{{< hint info >}}
Video Wall feature allows for a comprehensive visual overview of the construction site.
{{< /hint >}}

<p align="center">
<img src="/images/Videowall.png" width = 600 />
</p>

## How to Enable Video Wall

1. You must nominate a user to be upgraded through your Account Manager.

2. The Video wall feature will be added to the user’s account.

3. The upgraded user’s login details will be used to view the Video wall from any screen.

## Configuring the Video Wall

1. You can access the configuration step by going to *Video Wall* -> *Configure* (at the bottom right with the mechanical wheel icon).

<p align="center">
<img src="/images/configure_videowall.png" width = 600 />
</p>

2. The configuration dialog enables you to: 

    - Select the cameras you want to display on the grid.
    - Move the cameras around and resize them as needed.

<p align="center">
<img src="/images/select_camera.png" width = 600 />
</p>

<p align="center">
<img src="/images/customizable_grid.png" width = 600 />
</p>

3. Once saved, the grid configuration will be reflected in your Video Wall.

4. Additionally, the images are now fully displayed with all the timestamps visible.
