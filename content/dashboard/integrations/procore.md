---
title: Procore
type: docs
weight: 7
bookFlatSection: false
---

# Procore

**What is Procore?**

It is a Construction Project Management Platform widely used in the US.

<p align="center">
<img src="/images/procore.png" width = 200 />
</p>

**With the Evercam integration you can:**

- Enjoy a live view of your construction site in Procore to manage your site from anywhere.
- Record High-Resolution images for every second of the construction site progress.
- Highlight and communicate the key information by drawing and adding text to site images with our simple Mark-up tool.
- Compare before and after images from any point in time and create short clips using our compare tool.
- Keep track of progress by comparing live and recorded view with your 4D or 3D BIM Models.

**There are 3 ways Evercam can integrate with Procore:**

1. Send to Procore - Edit tool export
2. Snapmail
3. Add Evercam through the Procore Marketplace

## 1. Send to Procore

In the Evercam Dashboard, go to *Settings > Integrations*

<p align="center">
<img src="/images/procore_integration.png" width = 400 />
</p>

Select *Connect* under Procore and log in with your Procore credentials.

Once that is done, you'll be able to use the *Send to Procore* button in the edit tool to send images into a folder named *Evercam* in the project you choose.

<p align="center">
<img src="/images/send_to_procore.png" width = 400 />
</p>

## 2. Snapmail

When creating a Snapmail, you can select *Procore* as a Provider option

<p align="center">
<img src="/images/new_snapmail.png" width = 200 />
</p>

Once set up, Evercam will send images directly into the  project's photos section in Procore.

## 3. Add Evercam through the Procore Marketplace

Go to: https://marketplace.procore.com/apps/evercam

<p align="center">
<img src="/images/procore_marketplace.png" width = 400 />
</p>

Once you have installed the app, you will need to go into Procore and Select *App management*. There you will see *Evercam* appear in you apps list.

<p align="center">
<img src="/images/procore_app_management.png" width = 400 />
</p>

Select *View*.

<p align="center">
<img src="/images/procore_view.png" width = 400 />
</p>

Go to the *Configurations* tab and select *Create Configuration*.

<p align="center">
<img src="/images/procore_config.png" width = 400 />
</p>

*Global Projects Config* will let you access your cameras from all of your Procore Projects and 
*Company Level config* will let you select which projects you want to add Evercam to. For the 
title you can name it *Evercam - Live View* or something along those lines.

<p align="center">
<img src="/images/procore_create_config.png" width = 400 />
</p>

Once the configuration is created you will be able to access Evercam from Procore through the apps dropdown.

<p align="center">
<img src="/images/procore_evercam.png" width = 400 />
</p>