---
title: FAQs
weight: 7
---

# 360 FAQs

### **Why do I need 360?**
360 is good for visualizing interiors, as well as exterior spaces. It’s a tool that can be used 
for walkthroughs and presentations.

### **Which camera can I use to capture 360 images?**
We recommend using Insta360, but for now, we only support it.

### **Who does the capturing?**
360 captures will be care of the customer.

### **What are the other 360 platforms I can integrate with?**
We can integrate with Matterport, OpenSpace, and HoloBuilder.

### **Can I use point cloud scans?**
At the moment, we do not support LiDAR cameras and point cloud models.

### **How accurate is the measuring tool?**
Measurements will be very accurate if a reference measurement like the door height can be provided along with the video/ images so we can scale the model properly.